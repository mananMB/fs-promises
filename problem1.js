const fs = require("fs/promises");
const path = require("path");

// 1. Create a directory of random JSON files
// 2. Delete those files simultaneously

const problem1 = (directory) => {
  const generateFileList = () => {
    return Array(5)
      .fill(undefined)
      .map(() => {
        return `${~~(Math.random() * 100)}.json`;
      });
  };

  const fileList = generateFileList();

  const createDirectory = (directory) => {
    return fs.mkdir(path.join(__dirname + directory), { recursive: true });
  };

  const createFiles = (fileList) => {
    return Promise.all(
      fileList.map((file) => {
        return createFile(file);
      })
    );
  };

  const createFile = (file) => {
    return fs.writeFile(path.join(__dirname + directory, file), "", {
      flag: "wx",
    });
  };

  const deleteFiles = () => {
    return Promise.all(
      fileList.map((file) => {
        return deleteFile(file);
      })
    );
  };

  const deleteFile = (file) => {
    return fs.unlink(path.join(__dirname, directory, file));
  };

  createDirectory(directory)
    .then(() => {
      return createFiles(fileList);
    })
    .then(() => {
      return deleteFiles(fileList.push("as"));
    })
    .catch((reject) => {
      console.error(reject);
    });
};

module.exports = problem1;
