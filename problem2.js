const fs = require("fs/promises");
const readline = require("readline");

const problem2 = (file, filenamesFile) => {
  const readFile = (filepath) => {
    return fs.readFile(filepath, "utf-8");
  };

  const writeFile = (filepath, data) => {
    return fs.writeFile(filepath, data);
  };

  const appendFile = (filepath, data) => {
    return fs.appendFile(filepath, data);
  };

  const getLineFromFileData = (data, lineNumber) => {
    return data.split("\n")[lineNumber - 1];
  };

  const uppercase = (data) => {
    return data.toUpperCase();
  };

  const lowercaseAndSplit = (data) => {
    return data.toLowerCase().split(". ").join(".\n");
  };

  const sort = (data) => {
    return data.split("\n").sort().join("\n");
  };

  const deleteAllFiles = (filenames) => {
    return Promise.all(
      filenames.map((file) => {
        return fs.rm(file);
      })
    );
  };

  readFile(file)
    .then((response) => {
      return writeFile("uppercase.txt", uppercase(response));
    })
    .then(() => {
      return writeFile(filenamesFile, `${filenamesFile}\nuppercase.txt`);
    })
    .then(() => {
      return readFile(filenamesFile);
    })
    .then((response) => {
      return readFile(getLineFromFileData(response, 2));
    })
    .then((response) => {
      return writeFile("lowercase-split.txt", lowercaseAndSplit(response));
    })
    .then(() => {
      return appendFile(filenamesFile, "\nlowercase-split.txt");
    })
    .then(() => {
      return readFile(filenamesFile);
    })
    .then((response) => {
      return readFile(getLineFromFileData(response, 3));
    })
    .then((response) => {
      return writeFile("sorted.txt", sort(response));
    })
    .then(() => {
      return appendFile(filenamesFile, "\nsorted.txt");
    })
    .then(() => {
      return readFile(filenamesFile);
    })
    .then((response) => {
      return deleteAllFiles(response.split("\n"));
    })
    .catch((reject) => {
      console.error(reject);
    });
};

module.exports = problem2;
